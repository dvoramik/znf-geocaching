<?php
namespace App\Model;
require_once '../bootstrap.php';
use Nette\Caching\Storages\DevNullStorage;
use Nette\Database\Connection;
use Nette\Database\Context;
use Nette\Database\Structure;
use Nette\Database\Table\ActiveRow;
use Nette\Security\Passwords;
use Nette\Database;
use Nette\Utils\DateTime;
use Tester\Assert;
use Tester\Environment;
use Tester\TestCase;
/**
 * Class GeocacheModelTest
 * @package      App\Model
 * @testCase
 * @dataProvider ../database.ini
 */
class GeocacheModelTest extends TestCase
{

    /*
    insertUser($values)
    updateUser($id, $values)
    deleteUser($id)
    */

    /** @var Context */
    private $database;


    /** @var GeocacheModel */
    private $geocacheModel;
    /** */
    protected function setUp()
    {
        parent::setUp();
        $args = Environment::loadData();
        $connection = new Connection($args['dsn'], $args['user'], $args['password']);
        $this->database = new Context($connection, new Structure($connection, new DevNullStorage()));
        $this->geocacheModel = new GeocacheModel($this->database);
       
    }

    public function tearDown()
    {
    }

    /*
     * Testovani linku na kesku. Spatny title, keska zrusena, nebo geocaching ma jiny title stranek.
     */
    public function testGetTitleURL1(){
        Assert::same(' GC1NC5E Fantom RAJ (Unknown Cache) in Karlovarsky kraj, Czech Republic created by dítka ',$this->geocacheModel->getTitle('https://www.geocaching.com/geocache/GC1NC5E_fantom-raj'));
    }

     /*
     * Testovani linku na kesku. Spatny title, keska zrusena, nebo geocaching ma jiny title stranek.
     */
    public function testGetTitleURL2(){
        Assert::same(' GC13Y1B Letohradek a obora Hvezda (Multi-cache) in Hlavni mesto Praha, Czech Republic created by Fikus ',$this->geocacheModel->getTitle('https://www.geocaching.com/geocache/GC13Y1B_letohradek-a-obora-hvezda'));
    }


    /*
     * Testovani linku na kesku pres coordinf. Coordinf nefunguje.
     */
    public function testGetTitleCOORD1(){
        Assert::same(' GC2TWZ3 Horkovod (Unknown Cache) in Karlovarsky kraj, Czech Republic created by herrflick1044 ',$this->geocacheModel->getTitle('http://coord.info/'.'GC2TWZ3'));
    }


    /*
     * Testovani linku na kesku pres coordinf. Coordinf nefunguje.
     */
    public function testGetTitleCOORD2(){
      Assert::same(' GC154Z5 Jaroslav Foglar Geocache (Multi-cache) in Hlavni mesto Praha, Czech Republic created by szanto ',$this->geocacheModel->getTitle('http://coord.info/'.'GC154Z5'));
    }





}
(new GeocacheModelTest())->run();
