<?php

namespace App\Latte;

use Nette\Object;


class CoordFilter extends Object
{

    /**
     * @param string phone number $number
     * @return string converted phone number
     */
    public function __invoke($coords)

    {
        $pos = strpos($coords,'E');
        return substr($coords,0,$pos) . "\r\n" . substr($coords, $pos);	
    }
}