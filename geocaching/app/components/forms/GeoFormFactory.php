<?php

namespace App\Forms;



use Nette\Application\UI;

use App\Model\GeocacheModel;
use Nette\Forms\Form;

/**
 * Created by PhpStorm.
 * User: Kartol
 * Date: 13.04.2017
 * Time: 16:06
 */



class GeoFormFactory extends UI\Form
{
    /**
     * @var GeocacheModel
     */
    private $geocacheModel;

    /**
     * @var \Nette\Security\User
     */
    private $user;

    /**
     * GeoFormFactory constructor.
     * @param \Nette\Security\User $user
     * @param GeocacheModel $geocacheModel
     */
    public function __construct(\Nette\Security\User $user, GeocacheModel $geocacheModel)
    {

        $this->user = $user;
        $this->geocacheModel = $geocacheModel;
    }

    /**
     * adds common text fields gcKod, name, type, city, country, owner, latitude, longitude, user
     * and radio list logged for info about cache and finding
     * @param UI\Form $form
     */
    public function addCommonFields(UI\Form $form){
        $form->addText('gcKod','GC kód');
        $form->addText('name','Název');
        $form->addText('type','Typ');
        $form->addText('city','Město');
        $form->addText('country','Stát');
        $form->addText('owner','Majitel');

        $form->addText('latitude','Latitude');

        $form->addText('longitude','Longitude');


        $username = "Guest";
        if($this->user->getIdentity()){
            $username = $this->user->getIdentity()->getData()['username'];
        }

        $form->addText('user','Přidal')
            ->setDisabled(true)->setOmitted(false)
            ->setDefaultValue($username);
        $form->addRadioList('logged','Odloveno', [
            '1' => 'Ano',
            '0' => 'Ne',
        ])->setDefaultValue(0);
    }

    /**
     * Creates geocache form for adding cache
     * form has common fields and fields for getting info about cache through CURL function
     * this function is started by pressing send button which generates the info about cache
     * and button pridat for adding cache to data layer
     * @return UI\Form
     */
    public function createAddGeoForm()
    {
        $form = new UI\Form;
        $form->addProtection('Ochrana');

        /**
         * common fields for geocache info
         */
        $this->addCommonFields($form);

        $metody = [
            'link' => 'odkaz na kešku',
            'gcCode' => 'GC kód'
        ];

        $form->addSelect('metodaPridani','Přidat přes',$metody)
            ->setPrompt('Zvolte metodu přidání');
        $form->addText('text');
        $form->addSubmit('send','Získat informace');
        $form->addSubmit('pridat');
        $form->addHidden('userId')->setDefaultValue($this->user->getId());
        $form->onSuccess[] = [$this,'pressGetInfo'];
        return $form;
    }

    /**
     * Creates geocache form for editing cache
     * form has common fields and hidden field for user id
     * @return UI\Form
     */
    public function createEditGeoForm()
    {
        $form = new UI\Form;
        $form->addProtection('Ochrana');

        /**
         * common fields for geocache info
         */
        $this->addCommonFields($form);
        $form->addHidden('userId')->setDefaultValue($this->user->getId());

        $form->addSubmit('upravit','uprav');

        return $form;
    }

    /**
     * calls geocache model metods for getting info about geocache and sets values to common fields of form
     * @param Form $form
     * @param $values
     */
    public function pressGetInfo(Form $form, $values){
        if($form['send']->isSubmittedBy()){
            // hodit tuhle funkci do modelu a vratit geocache
            $geocache = [];
            if($form['metodaPridani']->value == 'link')
                $geocache = $this->geocacheModel->getByUrl($values->text);
            else if($form['metodaPridani']->value == 'gcCode')
                  $geocache = $this->geocacheModel->getByUrl('http://coord.info/'.$values->text);
            // ten tady predat
            $form->setValues($geocache);
        }
    }

}
