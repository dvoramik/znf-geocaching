<?php
/**
 * login logic mostly copied from some github repository
 */
namespace App\Forms;

use Nette;
use Nette\Security as NS;
use Nette\Application\UI\Form;


class LoginForm extends Nette\Object
{
    /** @var NS\User */
    private $user;
    /**
     * @param NS\User $user
     */

    /**
     * LoginForm constructor.
     * @param NS\User $user
     */
    public function __construct(NS\User $user)
    {
        $this->user = $user;
    }
    /**
     * Creates form for login
     * @return Form
     */
    public function create()
    {
        $form = new Form();
        $form->addText('username', 'Uživ. jméno')
            ->addRule(Form::FILLED, 'musíte zadat jméno');
        $form->addPassword('password', 'Heslo:')
            ->addRule(Form::FILLED, 'Musíte zadat heslo');
        $form->addSubmit('send','Přihlásit');
        $form->onSuccess[] = $this->loginProcess;
        return $form;
    }
    
    /**
     * Login process logic
     * @param Form $form
     * @param array $values
     *
     */
    public function loginProcess(Form $form, $values)
    {
        try {
            $this->user->login($values->username, $values->password);
        }
        catch (NS\AuthenticationException $e) {
            $form->addError($e->getMessage());
        }
    }
}