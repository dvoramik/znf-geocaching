<?php
/**
 * Created by PhpStorm.
 * User: Kartol
 * Date: 18.06.2017
 * Time: 14:12
 */

namespace App\Forms;

use App\Model\UserModel;
use Nette;
use Nette\Security as NS;
use Nette\Application\UI\Form;

class RegisterForm extends Nette\Object
{
    /**
     * @var UserModel
     */
    private $userModel;

    /**
     * dependency injection
     * @param UserModel $userModel
     */
    public function injectDependencies(
        UserModel $userModel
    )
    {
        $this->userModel= $userModel;
    }

    /**
     * creates register form
     * @return Form
     */
    public function create()
    {
        $form = new Form();
        $form->addText('username', 'Uživ. jméno')
            ->addRule(Form::FILLED, 'musíte zadat jméno');
        $form->addPassword('password', 'Heslo:')
            ->addRule(Form::FILLED, 'Musíte zadat heslo');
        $form->addSubmit('send','Registrovat');
        $form->onSuccess[] = $this->registerProcess;
        return $form;
    }

    /**
     * registers user filled from text field to data layer
     * @param $form
     * @param $values array of two values 'username' and 'password'
     */
    public function registerProcess($form,$values){
        try{
            $this->userModel->insertUser($values);

        }
        catch(\Exception $exception){
            $form->addError("Uzivatel jiz existuje");

        }
    }

}