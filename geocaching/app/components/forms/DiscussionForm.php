<?php

namespace App\Forms;



use Nette;
use App\Model\DiscussionModel;
use Nette\Application\UI\Form;
use Nette\Forms\Container;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;
use Nette\Forms\Controls\TextInput;

class DiscussionForm extends Nette\Object
{

    /**
      * discussion model for access to functions from data layer
      */
	private $discussionModel;

    /**
     * DiscussionForm constructor.
     * @param DiscussionModel $discussionModel
     */
	public function __construct(DiscussionModel $discussionModel)
    {
        $this->discussionModel = $discussionModel;
    }

    /**
     * Creates discussion form named 'discussionForm' with text field and submit button
     * @return Form
     */
     public function createDiscussionForm()
    {
        $form = new Form(NULL, 'discussionForm');
        $form->addProtection('Ochrana');
        $form->addText('text', 'Text')
            ->addRule(Form::FILLED, 'Vyplnte text');
        $form->addSubmit('send','Poslat');
        return $form;
    }




}