<?php

namespace App\Model;

use Tracy\Debugger;

use Nette\Utils\DateTime;


class DiscussionModel extends BaseModel
{


    /**
     * get discussion for geocache
     * @param $GCCode geocache
     * @return array|\Nette\Database\Table\IRow[]|\Nette\Database\Table\Selection discussion of geocache
     */
    public function getDiscussion($GCCode)
    {
		$selection = $this->database->table('Discussion');
		$selection->where("GCCode = ?", $GCCode);
		$selection->order("time");
        return $selection->fetchAll();
    }

    /**
     * inserts message to discussion
     * @param $GCCode geocache
     * @param $userID user
     * @param $text the message
     */
    public function	addMessage($GCCode,$userID,$text){
    	$selection = $this->database->table('Discussion');
    	$values =  array('time' =>  new DateTime(),
    					 'text' => $text,
    					 'GCCode' => $GCCode,
    					 'userID' => $userID);
        $selection->insert($values);

    }

}