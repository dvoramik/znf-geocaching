<?php
/**
 * Created by PhpStorm.
 * User: Kartol
 * Date: 02.05.2017
 * Time: 17:08
 */

namespace App\Model;


use Nette\Database\Table\IRow;
use Nette\Object;

/**
 * objekt reprezentujici kesku
 * objekt ma pole hodnot, ktere jsou v aplikaci pro kesku podstatne
 */
class Geocache extends Object
{


    /**
     * data o kesce
     */
    public $data = ['gcKod' => "",
                        'name' => "",
                        'type' => "",
                        'city' => "",
                        'country' => "",
                        'owner' => "",
                        'finalCoordinates' => ""
                    ];
    /**
     * Geocache constructor.
     * creates Geocache from active row
     * @param   irow of geocache
     */
    function __construct(IRow $row)
    {
        $this->data['gcKod'] = $row->GCCode;
        $this->data['name'] = $row->name;
        $this->data['type'] = $row->type->name;
        $this->data['city'] = $row->region->name;
        $this->data['country'] = $row->region->state->name;
        $this->data['owner'] = $row->owner->owner;
        $this->data['finalCoordinates'] = $row->finalCoordinates;
    }

}