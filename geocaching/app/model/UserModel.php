<?php

namespace App\Model;

use Nette\Security\Passwords;
use Tracy\Debugger;


class UserModel extends BaseModel
{


    /**
     * returns all users in array of active rows format
     * @return array|\Nette\Database\Table\IRow[]|\Nette\Database\Table\Selection
     */
    public function listUsers()
    {
        $selection = $this->database->table('User');
        return $selection->fetchAll();

    }

    /**
     * returns active row of user from its id
     * @param $id
     * @return bool|mixed|\Nette\Database\Table\IRow
     * @throws NoDataFound if user doesnt exist
     */
    public function getUser($id)
    {
        $ret = $this->database->table('User')->where(['id' => $id])->fetch();
        if(!$ret){
            throw new NoDataFound("Uzivatel neexistuje.");
        }
        return $ret;
        
    }

    /**
     * Inserts new user into db
     * @param array  $values
     * @return $id vloženého uživatele
     */
    public function insertUser($values)
    {
        // validate data
        // nick needed
        $values['password'] = Passwords::hash($values['password']);
        $selection = $this->database->table('User');
        return $selection->insert($values)->id;

    }

    /**
     * edits user
     * @param $id
     * @param $values
     * @return int
     * @throws NoDataFound if user doesnt exist
     */
    public function updateUser($id, $values)
    {
        $selection = $this->database->table('User');
        $selection->where("id=?",$id);
        $ret = $selection->update($values);
        if(!$ret){
            throw new NoDataFound("Uzivatel neexistuje.");
        }
        return $ret;

    }

    /**
     * deletes user
     * @param $id
     * @return int
     * @throws NoDataFound if user doesnt exist
     */
    public function deleteUser($id)
    {
        $selection = $this->database->table('User');
        $selection->where("id=?",$id);
        $ret = $selection->delete();
        if(!$ret){
            throw new NoDataFound("Uzivatel neexistuje.");
        }
        return $ret;

    }
}