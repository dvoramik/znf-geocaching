<?php
/**
 * Created by PhpStorm.
 * User: Kartol
 * Date: 15.04.2017
 * Time: 13:54
 */

namespace App\Model;


class OwnerModel extends BaseModel
{
    /**
     * return array of id and owner
     * @param bool $sort if true return is sorted order by owner names
     * @return array
     */
    public function listOwners($sort = false)
    {
        $selection = $this->database->table('Owner');
        if($sort){
            $selection->order('owner');
        }
        return $selection->fetchPairs('id','owner');

    }

    /**
     * return owner active row from owner id
     * @param $id
     * @return bool|mixed|\Nette\Database\Table\IRow owner active row
     * @throws NoDataFound if owner doesnt exist
     */
    public function getOwner($id)
    {
        $ret = $this->database->table('Owner')->where(['id' => $id])->fetch();
        if(!$ret){
            throw new NoDataFound("Owner neexistuje.");
        }
        return $ret;

    }

    /**
     * inserts user into data layer
     *
     * @param $values array with 'owner' parameter
     * @return mixed id of owner
     */
    public function insertOwner($values)
    {

        $selection = $this->database->table('Owner');
        return $selection->insert($values)->id;

    }


    /**
     * updates owners name
     * @param $id of owner
     * @param $values new name(owner) for owner
     * @return int number of updates rows
     * @throws NoDataFound if owner doesnt exist
     */
    public function updateOwner($id, $values)
    {
        $selection = $this->database->table('Owner');
        $selection->where("id=?",$id);
        $ret = $selection->update($values);
        if(!$ret){
            throw new NoDataFound("Owner neexistuje.");
        }
        return $ret;

    }

    /**
     * deletes owner
     * @param $id owners id
     * @return int
     * @throws NoDataFound if owner doesnt exist
     */
    public function deleteOwner($id)
    {
        $selection = $this->database->table('Owner');
        $selection->where("id=?",$id);
        $ret = $selection->delete();
        if(!$ret){
            throw new NoDataFound("Owner neexistuje.");
        }
        return $ret;

    }

    /**
     * from name of owner returns active row
     * @param $name name of owner
     * @return bool|mixed|\Nette\Database\Table\IRow active row of owner
     */
    public function getOwnerByName($name)
    {
        $ret = $this->database->table('Owner')->where(['owner' => $name])->fetch();
        if(!$ret)
            return false;
        return $ret;
    }

}