<?php

namespace App\Model;

use Tracy\Debugger;


class TypeModel extends BaseModel
{

    /**
     * returns all geocaches types in database in active row array
     * @return array|\Nette\Database\Table\IRow[]|\Nette\Database\Table\Selection
     */
    public function listAll(){
        return $this->database->table('Type')->fetchAll();
    }

    /**
     * returns only names of types
     * @return array of types names
     */
    public function listTypesNames(){
        return $this->database->table('Type')->fetchPairs('name','name');
    }

    /**
     * from id returns type active row
     * @param $id
     * @return bool|mixed|\Nette\Database\Table\IRow type active row
     * @throws NoDataFound if typ doesnt exist
     */
    public function getType($id)
    {
        $ret = $this->database->table('Type')->where(['id' => $id])->fetch();
        if(!$ret){
            throw new NoDataFound("Typ neexistuje.");
        }
        return $ret;

    }

    /**
     * returns active row of type from name
     * @param $name name of type
     * @return bool|mixed|\Nette\Database\Table\IRow type active row
     */
    public function getTypeByName($name)
    {
        $ret = $this->database->table('Type')->where(['name' => $name])->fetch();
        if(!$ret)
            return false;
        return $ret;
    }

    /**
     * inserts type into db
     * @param $name name of type
     * @return mixed
     */
    public function insertType($name)
    {
        return $this->database->table('Type')->insert($name)->id;
    }
  }