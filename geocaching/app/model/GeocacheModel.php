<?php

namespace App\Model;

use App\Model\OwnerModel;
use App\Model\TypeModel;
use App\Model\RegionModel;
use Nette\Database\IRow;
use Nette\Utils\DateTime;
use Tracy\Debugger;


class GeocacheModel extends BaseModel
{
    /**
     * @var \App\Model\OwnerModel
     */
    private $ownerModel;
    /**
     * @var \App\Model\TypeModel
     */
    private $typeModel;
    /**
     * @var \App\Model\RegionModel
     */
    private $regionModel;
    /**
     * @var \App\Model\LogModel
     */
    private $logModel;

    /**
     * depency injection
     * @param \App\Model\OwnerModel $ownerModel
     * @param \App\Model\RegionModel $regionModel
     * @param \App\Model\TypeModel $typeModel
     * @param LogModel $logModel
     */
    public function injectDependencies(
                OwnerModel $ownerModel,
                RegionModel $regionModel,
                TypeModel $typeModel,
                LogModel $logModel
    )
    {
        $this->ownerModel = $ownerModel;
        $this->regionModel = $regionModel;
        $this->typeModel = $typeModel;
        $this->logModel = $logModel;
    }

    /**
     * retrun array of active rows of all geocaches
     * @return array|\Nette\Database\Table\IRow[]|\Nette\Database\Table\Selection
     */
    public function listGeocaches()
    {
        $selection = $this->database->table('Geocache');
        return $selection->fetchAll();

    }

    /**
     * returns array of data with info about all geocaches from user`s view
     * its sql query and returns data witch names instead of ids
     * @param $userID
     * @return array|IRow[]|\Nette\Database\ResultSet
     */
    public function listGeocachesForUser($userID){
        $selection = $this->database->query("select Geocache.GCCode , timeAdded, Region.name AS mesto,Geocache.name, finalCoordinates, owner, (finder IS NOT NULL) AS found, State.name as zeme, Type.name as typ, username from Geocache left join (select GCCode as gc, userID as finder from Log where userID = ?) as l on Geocache.GCCode = l.gc inner join Owner on ownerID = Owner.id  inner join Region on regionID = Region.id inner join State on stateID = State.id inner join Type on typeID = Type.id inner join User on userID = User.id",$userID);
        return $selection->fetchAll();
    }

    /**
     * returns active row or array about geocache
     * from default it returns active row
     * if $row is false, returns array
     * @param $GCCode
     * @param bool $row
     * @return Geocache|bool|mixed|\Nette\Database\Table\IRow
     * @throws NoDataFound
     */
    public function getGeocache($GCCode,$row = true)
    {
        /** TODO */
        $selection = $this->database->table('Geocache');
        $selection->where("GCCode = ?", $GCCode);
        $ret = $selection->fetch(); 
        if(!$ret){
            throw new NoDataFound("Keska neexistuje.");
        }

        if($row)
             return $ret;

        return new Geocache($ret);
    }


    /**
     * inserts geocache into datalayer
     * expects data from Geocache class format
     * @param $values
     */
    public function insertGeocache($values)
    {
        // validate data
        // add timestamp

        $date = new DateTime();
        $logged = $values['logged'];

        // need ownerId
        $ownerId = $this->ownerModel->getOwnerByName($values['owner']);
        if($ownerId == false){
            $ownerId = $this->ownerModel->insertOwner(['owner' => $values['owner']]);
        }

        // region ID
        $regionId = $this->regionModel->getRegionByName($values['city']);
        if($regionId == false){
            $regionId = $this->regionModel->insertRegion(
                [
                    'region' => $values['city'],
                    'state' => $values['country']
                ]
            );
        }

        // type ID
        $typeId = $this->typeModel->getTypeByName($values['type']);
        if($typeId == false){
            $typeId = $this->typeModel->insertType(['name' => $values['type']]);
        }


        $selection = $this->database->table('Geocache');

        $values = [
            'GCCode' => $values['gcKod']
            , 'timeAdded' => $date
            , 'name' => $values['name']
            , 'finalCoordinates' => $values['latitude'].' '.$values['longitude']
            , 'ownerID' => $ownerId
            , 'regionID' => $regionId
            , 'userID' => $values['userId']
            , 'typeID' => $typeId
        ];
        $selection->insert($values)->id;
        if($logged)
            $this->logModel->logCache($values['userID'],$values['GCCode']);

    }

    /**
     * edits geocache
     * expects data in Geocache class format
     * @param $GCCode
     * @param $values data of Geocache class
     * @return int
     */
    public function updateGeocache($GCCode, $values)
    {
        // validate data
        if($values['logged'])
            $this->logModel->logCache($values['userId'],$values['gcKod']);
        else
            $this->logModel->deleteLog($values['userId'],$values['gcKod']);

        $selection = $this->database->table('Geocache');
        $selection->where(["GCCode" =>$GCCode]);
        echo $values['owner'];
        echo $GCCode;
        $ownerId = $this->ownerModel->getOwnerByName($values['owner']);
        if($ownerId == false){
            $ownerId = $this->ownerModel->insertOwner(['owner' => $values['owner']]);
        }

        // region ID
        $regionId = $this->regionModel->getRegionByName($values['city']);
        if($regionId == false){
            $regionId = $this->regionModel->insertRegion(
                [
                    'region' => $values['city'],
                    'state' => $values['country']
                ]
            );
        }

        // type ID
        $typeId = $this->typeModel->getTypeByName($values['type']);
        if($typeId == false){
            $typeId = $this->typeModel->insertType(['name' => $values['type']]);
        }

        $values = [
             'name' => $values['name']
            , 'finalCoordinates' => $values['latitude'].' '.$values['longitude']
            , 'ownerID' => $ownerId
            , 'regionID' => $regionId
            , 'userID' => $values['userId']
            , 'typeID' => $typeId
        ];



        $ret = $selection->update($values);
        return $ret;

    }

    /**
     * deletes geocache
     * @param $GCCode
     * @return int
     * @throws NoDataFound
     */
    public function deleteGeocache($GCCode)
    {
        $selection = $this->database->table('Geocache');
        $selection->where("GCCode = ? ", $GCCode);
        $ret = $selection->delete();
        if(!$ret){
            throw new NoDataFound("Keska neexistuje.");
        }
        return $ret;

    }


    /**
     * parses info about geocache from title
     * returns array with info about geocache in Geocache class format
     * @param $url url to geocaching.com website
     * @return array
     */
    public function getByUrl($url){

        $geocache = ['gcKod' => "",
            'name' => "",
            'type' => "",
            'city' => "",
            'country' => "",
            'owner' => ""
        ];


        // title html kodu kesky
        $title = $this->getTitle($url);


        // Parse title - GCCODE NAME (TYPE) in KRAJ, COUNTRY created by OWNER
        $title = trim($title);
        $pos = strpos($title, '(');
        $pos2 = strpos($title, ')');
        $pos3 = strpos($title, ',');
        $pos4 = strpos($title, 'created by');
        $pos5 = strpos($title, " ");

        $geocache['gcKod'] = substr($title, 0, $pos5);
        $geocache['name'] = substr($title,$pos5+1,$pos-$pos5-2);
        $geocache['type'] = substr($title,$pos+1,$pos2-$pos-1);
        $geocache['city'] = substr($title,$pos2+5,$pos3-$pos2-5);

        $geocache['country'] = substr($title,$pos3+2,$pos4-$pos3-3);

        $geocache['owner'] = substr($title,$pos4+11);
        return $geocache;
    }

    /**
     * should return title of geocache
     * @param $url url to geocaching.com website
     * @return mixed
     */
    public function getTitle($url)
    {
        // connect to geocaching com
        curl_setopt($ch=curl_init(), CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // allow redirects
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_MAXREDIRS,5); // return into a variable
        $response = curl_exec($ch);
        curl_close($ch);
        // v html kodu na geocachingu by meli v title kesky byt vsechny potrebne informace
        if(strlen($response)>0){
            $response = trim(preg_replace('/\s+/', ' ', $response)); // supports line breaks inside <title>
            preg_match("/\<title\>(.*?)\<\/title\>/i",$response,$title); // ignore case
            return $title[1];
        }
    }

    /**
     * returns decimal form of coordinates
     * eg. for N 49° 46.768 will return 49.77946
     * @param $text
     * @return int
     */
    private function convertToDecimal($text){
        // get degree, minute and seconds
        // N 49° 46.768
        $text = substr($text,2);
        $pos1 = strpos($text, '°');
        $pos2 = strpos($text,'.');

        $degrees = intval(substr($text,0,$pos1),10);
        $minutes = floatval(substr($text,$pos1+2));

        return $degrees + $minutes / 60;


    }

    /**
     * returns data about geocache for gpx format
     * @param $cache
     * @return string
     */
    public function getGPXData($cache){
        $geoCode = $cache->GCCode;
        $lat = $this->convertToDecimal(strstr($cache->finalCoordinates,' E',true));
        $lon = $this->convertToDecimal(strstr($cache->finalCoordinates,'E'));
        $owner = $cache->owner;
        $link = "http://coord.info/".$geoCode;
        $typ = $cache->typ;
        $name = $cache->name;
        $owner = str_replace ("&", "&amp;" ,  $owner  );
        $name = str_replace ("&",  "&amp;" ,$name  );
        $date = $cache->timeAdded;
        $poznamka  = '';

return '
<wpt lat="'.$lat.'" lon="'.$lon.'">
  <time>'.$date.'T00:00:00Z</time>
  <name>'.$geoCode.'</name>
  <link href="'.$link.'">
    <text>'.$name.'</text>
  </link>
  <sym>Geocache</sym>
  <type>Geocache|Unknown Cache</type>
  <groundspeak:cache id="368520" available="True" archived="False" xmlns:groundspeak="http://www.groundspeak.com/cache/1/0/1">
    <groundspeak:name>'.$name.'</groundspeak:name>
    <groundspeak:placed_by>'.$owner.'</groundspeak:placed_by>
    <groundspeak:owner>'.$owner.'</groundspeak:owner>
    <groundspeak:type>Unknown Cache</groundspeak:type>
    <groundspeak:container>Micro</groundspeak:container>
    <groundspeak:personal_note><![CDATA['.$poznamka.']]></groundspeak:personal_note>
  </groundspeak:cache>
</wpt>
<wpt lat="'.$lat.'" lon="'.$lon.'">
  <name>RX'.substr($geoCode,2).'</name>
  <desc><![CDATA[Původní souřadnice]]></desc>
  <sym>Reference Point</sym>
  <type>Waypoint|Reference Point</type>
</wpt>
<wpt lat="'.$lat.'" lon="'.$lon.'">
  <name>FI'.substr($geoCode,2).'</name>
  <desc><![CDATA[Final]]></desc>
  <sym>Final Location</sym>
  <type>Waypoint|Final Location</type>
</wpt>

';
    }


}