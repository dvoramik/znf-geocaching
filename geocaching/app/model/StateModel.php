<?php
/**
 * Created by PhpStorm.
 * User: Kartol
 * Date: 17.04.2017
 * Time: 16:37
 */

namespace App\Model;


use Nette;

class StateModel extends BaseModel
{
    /**
     * returns active row of state from name of state
     * @param $name name of state
     * @return bool|mixed|Nette\Database\Table\IRow active row
     */
    public function getStateByName($name)
    {
        $ret = $this->database->table('State')->where(['name' => $name])->fetch();
        if(!$ret)
            return false;
        return $ret;
    }

    /**
     * inserts state into db
     * @param $values
     * @return mixed id of state inserted
     */
    public function insertState($values)
    {
        return $this->database->table('State')->insert($values)->id;
    }

    /**returns array of all states names
     * @return array states names
     */
    public function listStatesNames(){
        return $this->database->table('State')->fetchPairs('name','name');
    }

    /**
     * return array of states names, sorted if $sort = true
     * @param bool $sort
     * @return array
     */
    public function listStates($sort = false)
    {
        $selection = $this->database->table('State');
        if($sort){
            $selection->order('name');
        }
        return $selection->fetchPairs('id','name');

    }
}