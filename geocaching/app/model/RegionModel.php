<?php
/**
 * Created by PhpStorm.
 * User: Kartol
 * Date: 15.04.2017
 * Time: 14:37
 */


namespace App\Model;

use App\Model\StateModel;

class RegionModel extends BaseModel
{
    /**
     * @var StateModel
     */
    private $stateModel;

    /**
     * dependency injection
     * @param \App\Model\StateModel $stateModel
     */
    public function injectDependencies(
        StateModel $stateModel
    )
    {
        $this->stateModel = $stateModel;
    }

    /**
     * returns array of regions
     * @return array of regions
     */
    public function listRegionsNames(){
        return $this->database->table('Region')->fetchPairs('name','name');
    }

    /**
     * from region names returns active row
     * if doesnt exist returns false
     * @param $name name of region
     * @return bool|mixed|\Nette\Database\Table\IRow active row
     */
    public function getRegionByName($name)
    {
        $ret = $this->database->table('Region')->where(['name' => $name])->fetch();
        if(!$ret)
            return false;
        return $ret;
    }

    /**
     * insert region into data layer
     * @param $values
     * @return mixed
     */
    public function insertRegion($values)
    {
        // if state exists, get state ID else insert state and get id -------
        $stateId = $this->stateModel->getStateByName($values['state']);
        if(!$stateId){
            $stateId = $this->stateModel->insertState(['name' => $values['state']]);
        }
        $values = [
            'name' => $values['region'],
            'stateID' => $stateId
        ];
        return $this->database->table('Region')->insert($values)->id;
    }

}