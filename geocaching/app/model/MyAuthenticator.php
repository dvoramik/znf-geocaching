<?php
namespace App\Model;
use Nette\Security as NS;
use Tracy\Debugger;

class MyAuthenticator extends BaseModel implements NS\IAuthenticator
{
    /**
     * atuhenticates user from credentials
     * cretentials needs to contain username and password
     *
     * @param array $credentials
     * @return NS\Identity indenty in guest role
     * @throws NS\AuthenticationException user not found or invalid password
     */
    function authenticate(array $credentials)
    {
        list($username, $password) = $credentials;
        $row = $this->database->table('User')
            ->where('username', $username)->fetch();

        if (!$row) {
            throw new NS\AuthenticationException('User not found.');
        }


        if (!NS\Passwords::verify($password, $row->password)) {
            throw new NS\AuthenticationException('Invalid password.');
        }


        return new NS\Identity($row->id, 'Guest', ['username' => $row->username]);
    }
}