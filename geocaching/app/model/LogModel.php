<?php

namespace App\Model;

use Tracy\Debugger;


class LogModel extends BaseModel
{
    /**
     * returns GCCodes of logged geocaches for user (id)
     * @param $id
     * @return array
     */
    public function listLogs($id)
    {
        $arr = array();
        
    	if(!$id)
    		return $arr;

        $selection = $this->database->table('Log');
        $selection->where("userID = ?", $id);
        $fetch = $selection->fetchAll();
        


        foreach ($fetch as $row) {
        	$arr[] = $row['GCCode'];
        }

        return $arr;

    }

    /**
     * returns 1 if geocache (GCCode) is logged for user (id)
     * @param $id
     * @param $GCCode
     * @return int
     */
    public function isLogged($id,$GCCode){
        $selection = $this->database->table('Log');
        $selection->where('GCCode = ? AND userID = ?', $GCCode,$id);
        $ret = $selection->fetch();
        if(!$ret){
            return 0;
        }
        return 1;
    }

    /**
     * logs geocache (GCCode) to user (id)
     * @param $id
     * @param $GCCode
     */
    public function logCache($id,$GCCode){
        $selection = $this->database->table('Log');
        try {
            $selection->insert(
                array(
                    'GCCode' => $GCCode,
                    'userID' => $id
                )
            );

        }
        catch(\Exception $exception){

        }
    }

    /**
     * deletes log (GCCode) for user (id)
     * @param $id
     * @param $GCCode
     */
    public function deleteLog($id,$GCCode){
        $selection = $this->database->table('Log');
        $selection->where('GCCode = ? AND userID = ?',$GCCode,$id);
        $selection->delete();
    }

}