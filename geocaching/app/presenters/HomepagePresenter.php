<?php

namespace App\Presenters;

use App\Forms\FilterFormFactory;
use App\Model\GeocacheModel;
use App\Model\RegionModel;
use App\Model\StateModel;
use App\Model\UserModel;
use App\Model\TypeModel;
use App\Model\LogModel;
use Nette\Application\Responses\FileResponse;
use Ublaboo\DataGrid\DataGrid;



class HomepagePresenter extends BasePresenter
{
    /**
     * @var GeocacheModel
     */
	private $geocacheModel;
    /**
     * @var UserModel
     */
    private $userModel;
    /**
     * @var TypeModel
     */
    private $typeModel;

    /**
     * @var RegionModel
     */
  private $regionModel;
    /**
     * @var StateModel
     */
  private $stateModel;
    /**
     * @var LogModel
     */
  private $logModel;

    /**
     * dependency injection
     * @param GeocacheModel $geocacheModel
     * @param UserModel $userModel
     * @param TypeModel $typeModel
     * @param LogModel $logModel
     * @param RegionModel $regionModel
     * @param StateModel $stateModel
     */
	public function injectDependencies(
        GeocacheModel $geocacheModel,
        UserModel $userModel,
        TypeModel $typeModel,
        LogModel $logModel,
        RegionModel $regionModel,
        StateModel $stateModel

    )
    {
        $this->geocacheModel = $geocacheModel;
        $this->userModel = $userModel;
        $this->typeModel = $typeModel;
        $this->logModel = $logModel;
        $this->regionModel = $regionModel;
        $this->stateModel = $stateModel;
    }

    /**
     * renders datagrid for currently logged user
     */
    public function renderDefault() {
       $geocaches = $this->geocacheModel->listGeocaches();

       /* returns logged caches for currently logged user */
       $userID = $this->getUser()->isLoggedIn()?$this->getUser()->getID():NULL;
       $loggedOnes = $this->logModel->listLogs($userID);

       $this->template->geocaches = $geocaches;
       $this->template->loggedOnes = $loggedOnes;
    }


    /**
     * logs geocaches selected from datagrid action
     * @param array $GCCodes
     */
    public function logCaches(array $GCCodes){

        foreach ($GCCodes as $GCCode)
            $this->logModel->logCache($this->getUser()->getId(),$GCCode);

        $this->redirect('this');
    }

    /**
     * log one geocache from datagrid action
     * @param $GCCode
     */
    public function logCache($GCCode){


        $this->logModel->logCache($this->getUser()->getId(),$GCCode);

        $this->redirect('this');
    }


    /**
     * creates datagrid with all informations about geocaches for logged user
     * has file respond with .gpx format of currently filtered geocaches
     * actions for logging caches
     * @param $name
     * @return static
     */
    public function createComponentDatagrid($name)
    {
        $grid = (new Datagrid($this,$name))
                ->setColumnsHideable();



        $grid->setPrimaryKey('GCCode');


        $grid->setDataSource($this->geocacheModel->listGeocachesForUser($this->getUser()->getId()));


        $grid->addColumnText('GCCode','Gc kód')
            ->setTemplate(__DIR__ . '/templates/Datagrid/gccode.latte')
            ->setSortable();
        $grid->addColumnText('QRCode','QR')
            ->setTemplate(__DIR__.'/templates/Datagrid/qrcode.latte');
        $grid->addColumnStatus('found','Odloveno')
            ->setSortable()
            ->addOption(0, 'Ne')
            ->setClass('btn-danger')
            ->endOption()
            ->addOption(1, 'Ano')
            ->setClass('btn-success')
            ->endOption()
            ->onChange[] = [$this, 'logCache'];
        $grid->addColumnText('typ','Typ')
            ->setSortable();
        $grid->addColumnText('name','Název')
            ->setSortable();
        $grid->addColumnText('owner','Majitel')
            ->setSortable();
        $grid->addColumnText('zeme','Země')
            ->setSortable();
        $grid->addColumnText('mesto','Město')
            ->setSortable();
        $grid->addColumnText('username','Vložil')
            ->setSortable();
        $grid->addColumnDateTime('timeAdded', 'Přidáno')
            ->setSortable();
        $grid->addColumnText('finalCoordinates','Vypočítané souřadnice')
            ->setRenderer(function($item) {
                $pos = strpos($item->finalCoordinates,'E');
                return substr($item->finalCoordinates,0,$pos) . ' '. substr($item->finalCoordinates, $pos);
            });


        $grid->addFilterText('owner', 'Search', ['owner']);


        $grid->addFilterDate('timeAdded', 'Přidáno:');
        $grid->addFilterText('username', 'Search', ['username']);

        $grid->addFilterText('name', 'Search', ['name']);
        $grid->addFilterText('GCCode', 'Search', ['GCCode']);


        $grid->addFilterMultiSelect('typ', 'Typ:', $this->typeModel->listTypesNames(),'typ');



        $grid->addFilterMultiSelect('found', 'Odloveno:', [
            0 => 'Ne',
            1 => 'Ano'
        ],'found');


        $grid->addFilterMultiSelect('mesto','Mesto:',$this->regionModel->listRegionsNames());
        $grid->addFilterMultiSelect('zeme','Zeme:',$this->stateModel->listStatesNames());

       $grid->addAction('show', 'zobraz', 'Geocache:show')
            ->setTitle('Show');
        $grid->addAction('delete', 'delete', 'Geocache:delete')
            ->setTitle('Delete');
        $grid->addAction('edit', 'uprav', 'Geocache:edit')
            ->setTitle('Edit');


        $grid->addGroupAction('Odloveno')->onSelect[] = [$this, 'logCaches'];

        $grid->addExportCallback('Vygeneruj GPX', function($data_source, $grid) {
            $myfile =  fopen("download.gpx", "w");
            fwrite($myfile, '<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<gpx version="1.1" creator="Locus Map, Android"
 xmlns="http://www.topografix.com/GPX/1/1"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd"
 xmlns:gpx_style="http://www.topografix.com/GPX/gpx_style/0/2"
 xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3"
 xmlns:gpxtrkx="http://www.garmin.com/xmlschemas/TrackStatsExtension/v1"
 xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v2"
 xmlns:locus="http://www.locusmap.eu">
  <metadata>
    <desc>File with points/tracks from Locus Map/3.20.1</desc>
  </metadata>');
            foreach($data_source as $GCCode)
                fwrite($myfile, $this->geocacheModel->getGPXData($GCCode));

            fwrite($myfile, '</gpx>');
            fclose($myfile);
            $this->sendResponse(new FileResponse( 'download.gpx'));
            exit(0);
        },true);


        return $grid;
    }

}
