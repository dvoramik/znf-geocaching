<?php

namespace App\Presenters;


use Nette;
use App\Model;
use App\Model\UserModel;


/**
 * Base presenter for all application presenters.
 */
class BasePresenter extends Nette\Application\UI\Presenter
{
    /**
     * @var UserModel
     */
	 private $userModel;

    /**
     * BasePresenter constructor.
     * @param UserModel $userModel
     */
	 public function __construct(UserModel $userModel) {
        $this->userModel = $userModel;
    }

    /**
     * redirect to login page, if user isnt logged in
     */
    protected function startup()
    {
        parent::startup();
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('Login:');
        }
    }

    /**
     * set username for template rendering
     */
    public function beforeRender() {
	 	if($this->getUser()->getIdentity())
	 		$this->template->username = $this->getUser()->getIdentity()->getData()['username'];
	 	else
	 		$this->template->username = "";
	 }
}
