<?php

namespace App\Presenters;

use App\Forms\GeoFormFactory;
use App\Model\Geocache;
use Nette;
use App\Model\GeocacheModel;
use App\Model\UserModel;
use App\Model\TypeModel;
use App\Model\LogModel;
use App\Model\DiscussionModel;
use App\Forms\DiscussionForm;
use Nette\Application\UI;

class GeocachePresenter extends BasePresenter
{
    /**
     * @var GeocacheModel
     */
    private $geocacheModel;
    /**
     * @var UserModel
     */
  private $userModel;
    /**
     * @var TypeModel
         */
  private $typeModel;
    /**
     * @var LogModel
     */
  private $logModel;
    /**
     * @var DiscussionModel
     */
  private $discussionModel;
    /**
     * @var DiscussionForm
     */
  private $discussionForm;
  /** @var  GeoFormFactory */
  private $geoFormFactory;

    /**
     * dependency injection
     * @param GeocacheModel $geocacheModel
     * @param UserModel $userModel
     * @param TypeModel $typeModel
     * @param LogModel $logModel
     * @param DiscussionModel $discussionModel
     * @param DiscussionForm $discussionForm
     * @param GeoFormFactory $geoFormFactory
     */
	public function injectDependencies(
        GeocacheModel $geocacheModel,
        UserModel $userModel,
        TypeModel $typeModel,
        LogModel $logModel,
        DiscussionModel $discussionModel,
        DiscussionForm $discussionForm,
        GeoFormFactory $geoFormFactory

    )
    {
        $this->geocacheModel = $geocacheModel;
        $this->userModel = $userModel;
        $this->typeModel = $typeModel;
        $this->logModel = $logModel;
        $this->discussionModel = $discussionModel;
        $this->discussionForm = $discussionForm;
        $this->geoFormFactory = $geoFormFactory;
    }

    /**
     * redirects to homepage if no action specified
     */
    public function renderDefault() {
      $this->redirect('Homepage:');
    }

    /**
     * renders page for show geocache
     * contains geocache info and discussion
     * @param $GCCode
     */
    public function renderShow($GCCode){

        $this->template->g = $this->geocacheModel->getGeocache($GCCode);
        $discussion = $this->discussionModel->getDiscussion($GCCode);
        $this->template->discussion = $discussion;
        $this->template->pocet = sizeOf($discussion);

    }



    public function actionShow($GCCode)
    {


    }

    /**
     * renders page for editing geocache
     * @param $GCCode
     */
    public function renderEdit($GCCode){
        $form = $this['editGeoForm'];
        $cache = $this->geocacheModel->getGeocache($GCCode,false)->data;
        $cache['logged'] = $this->logModel->isLogged($this->getUser()->getId(),$GCCode);
        // print_r($cache);
        $cache['latitude'] = strstr($cache['finalCoordinates'],' E',true);
        $cache['longitude'] = strstr($cache['finalCoordinates'],'E');
        $form->setValues($cache);

    }

    /**
     * deletes geocache from db and redirects to homepage
     * @param $GCCode
     */
    public function actionDelete($GCCode)
    {
        $this->geocacheModel->deleteGeocache($GCCode);
        $this->redirect('Homepage:');
    }


    public function actionEdit($GCCode)
    {
    }
    public function  actionAdd(){


    }

    /**
     * form action for inserting geocache into database
     * @param UI\Form $form
     * @param $values
     */
    public function pressAdd(Nette\Application\UI\Form $form, $values){
        if($form['pridat']->isSubmittedBy()){
            $this->geocacheModel->insertGeocache($values);
            // tady dat normalni redirect
            $this->redirect('Homepage:');
        }
    }

    /**
     * form action to editing geocache in database
     * @param $form
     * @param $values
     */
    public function pressEdit($form,$values){
            // update nad databazi a homepage
            //  $this->geocacheModel->updateGeocache($values['gcKod'],$values);
        if($form['upravit']->isSubmittedBy()) {
            $this->geocacheModel->updateGeocache($values['gcKod'],$values);
            $this->redirect('Homepage:');
        }


    }


    /**
     * creates discussion form
     * @return UI\Form
     */
    public function createComponentDiscussionForm()
    {
        $form = $this->discussionForm->createDiscussionForm();
        $form->addHidden('GCCode')
              ->setValue($this->params['GCCode'])
               ->setRequired('neznam gc kod');
        $form->onSuccess[] = function($form,$values){
            $this->discussionModel->addMessage($values->GCCode,$this->getUser()->getID(),$values->text);
        };
        $form->onSuccess[] = function ($form) {
                $this->redirect("Geocache:show",  array('GCCode' => $this->params['GCCode'] ));
            };
        return $form;
    }

    /**
     * creates add form
     * @return UI\Form
     */
    public function createComponentAddGeoForm()
    {

        $form = $this->geoFormFactory->createAddGeoForm();

        $form->onSuccess[] = [$this,'pressAdd'];

        return $form;
    }

    /**
     * creates edit form
     * @return UI\Form
     */
    public function createComponentEditGeoForm()
    {
        $form = $this->geoFormFactory->createEditGeoForm();


        $form->onSuccess[] = [$this, 'pressEdit'];
        return $form;
    }




}
