<?php


namespace App\Presenters;

use App\Forms\RegisterForm;
use Nette;
use App\Forms\LoginForm;
use App\Model;
use App\Model\GeocacheModel;
use App\Model\UserModel;
use App\Model\TypeModel;



class LoginPresenter extends Nette\Application\UI\Presenter
{
    /**
     * @var LoginForm
     */
    public $loginForm;
    /**
     * @var RegisterForm
     */
    public $registerForm;

    /**
     * dependency injection
     * @param LoginForm $loginForm
     * @param RegisterForm $registerForm
     */
    public function injectDependencies(
        LoginForm $loginForm,
        RegisterForm $registerForm
    )
    {
        $this->loginForm = $loginForm;
        $this->registerForm = $registerForm;
    }

    /**
     * creates login form
     * @return Nette\Application\UI\Form
     */
    public function createComponentLogin()
    {
        $form = $this->loginForm->create();
        $form->onSuccess[] = function ($form)
        {
            $this->redirect('Homepage:');
        };
        return $form;
    }

    /**
     * creates register form
     * @return Nette\Application\UI\Form
     */
    public function createComponentRegister()
    {
        $form = $this->registerForm->create();
        $form->onSuccess[] = function ($form)
        {
            $this->redirect('Homepage:');
        };
        return $form;
    }

    /**
     * logs out currently logged user
     */
    public function actionOut()
    {
        $this->getUser()->logout(TRUE);
        $this->flashMessage('Odhlášení bylo úspěšné.');
        $this->redirect('Homepage:');
    }

    /**
     * sets username for template
     */
    public function beforeRender()
    {
        parent::beforeRender();
        if($this->getUser()->getIdentity())
            $this->template->username = $this->getUser()->getIdentity()->getData()['username'];
        else
            $this->template->username = "";
    }
}