/*
CREATE TABLE `User`
(
	`userID` INTEGER NOT NULL,
	`nick` TEXT NOT NULL,
	CONSTRAINT `PK_User` PRIMARY KEY (`userID`)
)

;
*/

/*  create users  */

INSERT INTO `User` (`username`,`password`) VALUES ('Scoffer','213');
INSERT INTO `User` (`username`,`password`) VALUES ('Somrak','213');



/*
CREATE TABLE `State`
(
	`stateID` INT NOT NULL AUTO_INCREMENT,
	`name` TEXT NOT NULL,
	CONSTRAINT `PK_State` PRIMARY KEY (`stateID`)
)

;

*/

INSERT INTO `State` (`name`) VALUES ('Česká republika');
/*

CREATE TABLE `Region`
(
	`regionID` INT NOT NULL AUTO_INCREMENT,
	`name` TEXT NOT NULL,
	`stateID` INT 	 NULL,
	CONSTRAINT `PK_Region` PRIMARY KEY (`regionID`)
)

;


*/

INSERT INTO `Region` (`name`,`stateID`) VALUES ('Praha',1);


/*

CREATE TABLE `Owner`
(
	`owner` TEXT NOT NULL,
	`ownerID` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT `PK_Owner` PRIMARY KEY (`ownerID`)
)

;

*/

INSERT INTO `Owner` (`owner`) VALUES ('tucnak');


/*
CREATE TABLE `Type`
(
	`type` INT NOT NULL AUTO_INCREMENT,

	CONSTRAINT `PK_Type` PRIMARY KEY (`type`)
)

;
*/
/* mystery -- add uniq name */
INSERT INTO `Type` (`name`) VALUES ('traditional');

/*

CREATE TABLE `Geocache`
(
	`GCCode` VARCHAR(8) NOT NULL,
	`timeAdded` TIMESTAMP NOT NULL,
	`name` TEXT NOT NULL,
	`finalCoordinates` TEXT 	 NULL,
	`startCoordinates` TEXT NOT NULL,
-/  `ownerID` INT NOT NULL,
-/  `regionID` INT NOT NULL,
-/  `userID` INTEGER NOT NULL,
-/  `type` INT NOT NULL,
	CONSTRAINT `PK_Geocache` PRIMARY KEY (`GCCode`)
)

;

*/
INSERT INTO `Geocache` (`GCCode`,`timeAdded`,`name`,`finalCoordinates`,
		`startCoordinates`,`ownerID`,`regionID`,`userID`,`typeID`) VALUES
	('GC16W2R',NOW(),'Dusickova','N 50° 08.259 E 014° 28.744','N 50° 08.259 E 014° 28.744',1,1,1,1);



INSERT INTO `Geocache` (`GCCode`,`timeAdded`,`name`,`finalCoordinates`,
		`startCoordinates`,`ownerID`,`regionID`,`userID`,`typeID`) VALUES
	('GC11111',NOW(),'Dusickova2','N 50° 08.259 E 014° 28.744','N 50° 08.259 E 014° 28.744',1,1,2,1);


INSERT INTO `Geocache` (`GCCode`,`timeAdded`,`name`,`finalCoordinates`,
		`startCoordinates`,`ownerID`,`regionID`,`userID`,`typeID`) VALUES
	('GC12345',NOW(),'Dusickova3','N 50° 08.259 E 014° 28.744','N 50° 08.259 E 014° 28.744',1,1,1,1);

insert into Log (`GCCode`, `UserID`) VALUES ("GC16W2R",1);
insert into Log (`GCCode`, `UserID`) VALUES ("GC12345",1);
insert into Log (`GCCode`, `UserID`) VALUES ("GC11111",2);


INSERT INTO Discussion (`time`,`text`,`GCCode`,`userID`) VALUES (NOW(),"keska byla pod kamenem u smrku","GC11111",1);

INSERT INTO Discussion (`time`,`text`,`GCCode`,`userID`) VALUES (NOW()+1,"Diky, byla tam","GC11111",2);
