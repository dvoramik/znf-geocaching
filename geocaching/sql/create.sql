/* ---------------------------------------------------- */
/*  Generated by Enterprise Architect Version 12.0 		*/
/*  Created On : 20-b�e-2017 12:05:40 				*/
/*  DBMS       : MySql 						*/
/* ---------------------------------------------------- */


SET NAMES utf8;
SET FOREIGN_KEY_CHECKS=0 ;

/* Drop Tables */

DROP TABLE IF EXISTS `User` CASCADE
;

DROP TABLE IF EXISTS `Type` CASCADE
;

DROP TABLE IF EXISTS `State` CASCADE
;

DROP TABLE IF EXISTS `Region` CASCADE
;

DROP TABLE IF EXISTS `Owner` CASCADE
;

DROP TABLE IF EXISTS `Log` CASCADE
;

DROP TABLE IF EXISTS `Geocache` CASCADE
;

DROP TABLE IF EXISTS `Discussion` CASCADE
;

/* Create Tables */

CREATE TABLE `User`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(60) BINARY NOT NULL,
	`password` VARCHAR(64) NOT NULL,
	CONSTRAINT `PK_User` PRIMARY KEY (`id`),
	CONSTRAINT `UC_User` UNIQUE KEY (`username`)
)DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci

;

CREATE TABLE `Type`
(
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(35) NOT NULL,
	CONSTRAINT `PK_Type` PRIMARY KEY (`id`),
	CONSTRAINT `UC_Type` UNIQUE KEY (`name`)
)DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci
;

CREATE TABLE `State`
(
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL,
	CONSTRAINT `PK_State` PRIMARY KEY (`id`),
	CONSTRAINT `UC_State` UNIQUE KEY (`name`)
)DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci
;

CREATE TABLE `Region`
(
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL,
	`stateID` INT 	 NULL,
	CONSTRAINT `PK_Region` PRIMARY KEY (`id`),
	CONSTRAINT `UC_Region` UNIQUE KEY (`name`)
)DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci
;

CREATE TABLE `Owner`
(
	`owner` VARCHAR(60) BINARY NOT NULL,
	`id` INT NOT NULL AUTO_INCREMENT,
	CONSTRAINT `PK_Owner` PRIMARY KEY (`id`),
	CONSTRAINT `UC_Owner` UNIQUE KEY (`owner`)
)DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci
;

CREATE TABLE `Log`
(
	`GCCode` VARCHAR(8) NOT NULL,
	`userID` INTEGER 	 NULL,
	 CONSTRAINT UC_Log UNIQUE (`GCCode`,`userID`)
)DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci
;

CREATE TABLE `Geocache`
(
	`GCCode` VARCHAR(8) NOT NULL,
	`timeAdded` TIMESTAMP NOT NULL,
	`name` TEXT NOT NULL,
	`finalCoordinates` TEXT NULL,
	`ownerID` INT NOT NULL,
	`regionID` INT NOT NULL,
	`userID` INTEGER NOT NULL,
	`typeID` INT NOT NULL,
	CONSTRAINT `PK_Geocache` PRIMARY KEY (`GCCode`)
)DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci
;

CREATE TABLE `Discussion`
(
	`time` TIMESTAMP NOT NULL,
	`text` TEXT NOT NULL,
	`GCCode` VARCHAR(8) NOT NULL,
	`userID` INTEGER NOT NULL,
	CONSTRAINT `PK_DISCUSSION` PRIMARY KEY (`GCCode`,`userID`,`time`)
)DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci
;

/* Create Foreign Key Constraints */

ALTER TABLE `Region` 
 ADD CONSTRAINT `FK_Region_State`
	FOREIGN KEY (`stateID`) REFERENCES `State` (`id`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `Log` 
 ADD CONSTRAINT `FK_Log_Geocache`
	FOREIGN KEY (`GCCode`) REFERENCES `Geocache` (`GCCode`) ON DELETE CASCADE ON UPDATE Restrict
;

ALTER TABLE `Log` 
 ADD CONSTRAINT `FK_Log_User`
	FOREIGN KEY (`userID`) REFERENCES `User` (`id`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `Geocache` 
 ADD CONSTRAINT `FK_Geocache_Owner`
	FOREIGN KEY (`ownerID`) REFERENCES `Owner` (`id`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `Geocache` 
 ADD CONSTRAINT `FK_Geocache_Region`
	FOREIGN KEY (`regionID`) REFERENCES `Region` (`id`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `Geocache` 
 ADD CONSTRAINT `FK_Geocache_Type`
	FOREIGN KEY (`typeID`) REFERENCES `Type` (`id`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `Geocache` 
 ADD CONSTRAINT `FK_Geocache_User`
	FOREIGN KEY (`userID`) REFERENCES `User` (`id`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `Discussion` 
 ADD CONSTRAINT `FK_Discussion_Geocache`
	FOREIGN KEY (`GCCode`) REFERENCES `Geocache` (`GCCode`) ON DELETE CASCADE ON UPDATE Restrict
;

ALTER TABLE `Discussion` 
 ADD CONSTRAINT `FK_Discussion_User`
	FOREIGN KEY (`userID`) REFERENCES `User` (`id`) ON DELETE Restrict ON UPDATE Restrict
;

SET FOREIGN_KEY_CHECKS=1 ;
