var indexSectionsWithContent =
{
  0: "$5_abcdeghilmnoprstu",
  1: "bcdeghilmnorstu",
  2: "a",
  3: "5bcdeghlmorstu",
  4: "_abcdgilprsu",
  5: "$"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables"
};

